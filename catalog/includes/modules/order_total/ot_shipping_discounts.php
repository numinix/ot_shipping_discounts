<?php
/**
 * @package order_total
 * @copyright Copyright 2007-2008 Numinix http://www.numinix.com
 * @copyright Portions Copyright 2003-2008 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ot_shipping_discounts.php 10 2011-09-16 05:11:36Z numinix $
 */
 if (!IS_ADMIN_FLAG) {
  require_once('includes/classes/http_client.php');
 } 
  class ot_shipping_discounts {
    var $code, $title, $description, $enabled, $sort_order, $output, 
        $shipping_mods, $discount_table, $discount_amount, $method_id, $discount_type;
   
    function __construct($shipping_id = false, $shipping_cost = false) {
      global $order;
      
      // comment out this line, for testing only
      $_SESSION['payment_attempt'] = 0;
      
      $this->code = 'ot_shipping_discounts';
      $this->title = MODULE_SHIPPING_DISCOUNTS_TITLE;
      $this->description = MODULE_SHIPPING_DISCOUNTS_DESCRIPTION;
      $this->enabled = defined('MODULE_SHIPPING_DISCOUNTS_STATUS') ? MODULE_SHIPPING_DISCOUNTS_STATUS : "";
      $this->sort_order = defined('MODULE_SHIPPING_DISCOUNTS_SORT_ORDER') ? (int)MODULE_SHIPPING_DISCOUNTS_SORT_ORDER : null; ;
      $this->output = array();
      //$this->recalculate_tax = MODULE_SHIPPING_DISCOUNTS_CALC_TAX;
      $this->shipping_mods = $this->build_shipping_modules_list();
      $this->discount_type = defined('MODULE_SHIPPING_DISCOUNTS_TOTAL_TYPE') ? (int)MODULE_SHIPPING_DISCOUNTS_TOTAL_TYPE : null;
      //$this->discount_type = 'order_total';
      
      if (!IS_ADMIN_FLAG && $this->enabled) {
        if (!$shipping_id) {
          $shipping_id = $_SESSION['shipping']['id'];
          //$shipping_id = explode('_', $shipping_id);
          //$shipping_id = $shipping_id[0];
          //echo $shipping_id;
          //die();
        }
        if (!$shipping_cost) {
          $shipping_cost = $_SESSION['shipping']['cost'];
        }
        
        $this->total_type = MODULE_SHIPPING_DISCOUNTS_TOTAL_TYPE;
        $this->discount_table = $this->get_shipping_module($this->shipping_mods, $shipping_id);
        $this->discount_percentage = $this->get_discount_percentage($this->discount_table);
        $this->discount_amount = $this->get_discount_amount($shipping_cost);
        if ($this->discount_amount <= 0) {
          $this->enabled = false;
        }
        
        /*
        if ($_GET['main_page'] == FILENAME_CHECKOUT_PROCESS) {
          echo 'Shipping ID: ' . $shipping_id . '<br />';
          echo 'Shipping Cost: ' . $shipping_cost . '<br />';
          echo 'Discount Table: ' . $this->discount_table . '<br />';
          echo 'Discount %: ' . $this->discount_percentage . '<br />';
          echo 'Discount Amount: ' . $this->discount_amount . '<br />';
          echo 'Enabled: ' . $this->enabled . '<br />';
          die();
        }
        */       
        
        if ($this->enabled) {
          if ($_GET['main_page'] != 'shopping_cart') {
            $module = substr($shipping_id, 0, strpos($shipping_id, '_'));
            if ($GLOBALS[$module]->tax_class > 0) {
              if (!defined($GLOBALS[$module]->tax_basis)) {
                $shipping_tax_basis = STORE_SHIPPING_TAX_BASIS;
              } else {
                $shipping_tax_basis = $GLOBALS[$module]->tax_basis;
              }
                
              if ($shipping_tax_basis == 'Billing') {
                $shipping_tax = zen_get_tax_rate($GLOBALS[$module]->tax_class, $order->billing['country']['id'], $order->billing['zone_id']);
                $shipping_tax_description = zen_get_tax_description($GLOBALS[$module]->tax_class, $order->billing['country']['id'], $order->billing['zone_id']);
              } elseif ($shipping_tax_basis == 'Shipping') {
                $shipping_tax = zen_get_tax_rate($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
                $shipping_tax_description = zen_get_tax_description($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
              } else {
                if (STORE_ZONE == $order->billing['zone_id']) {
                  $shipping_tax = zen_get_tax_rate($GLOBALS[$module]->tax_class, $order->billing['country']['id'], $order->billing['zone_id']);
                  $shipping_tax_description = zen_get_tax_description($GLOBALS[$module]->tax_class, $order->billing['country']['id'], $order->billing['zone_id']);
                } elseif (STORE_ZONE == $order->delivery['zone_id']) {
                  $shipping_tax = zen_get_tax_rate($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
                  $shipping_tax_description = zen_get_tax_description($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
                } else {
                  $shipping_tax = 0;
                }
              }
              $shipping_tax_discount = round(zen_calculate_tax($this->discount_amount, $shipping_tax), 2);
              $order->info['tax'] -= $shipping_tax_discount;
              $order->info['tax_groups']["$shipping_tax_description"] -= $shipping_tax_discount;
              $order->info['total'] -= $shipping_tax_discount;
              //$_SESSION['shipping_tax_description'] = $shipping_tax_description;
              $_SESSION['shipping_tax_amount'] -=  $shipping_tax_discount;
              if (DISPLAY_PRICE_WITH_TAX == 'true') $order->info['shipping_cost'] -= $shipping_tax_discount;
            }
          }
        }
      }
    }
    
    function process() {
      global $order, $currencies, $ot_subtotal;
      if ($this->enabled) {
        //switch ($this->discount_type) {
          //case ('order-total'):
            $this->output[] = array('title' => $this->title . ' (' . $this->discount_percentage * 100 . '%):',
                                    'text' => '-' . $currencies->format($this->discount_amount),
                                    'value' => $this->discount_amount);
            //break;
          //case ('sub-total'): 
            //$order->info['total'] -= $this->discount_amount;
            //break;
        //}
        $order->info['total'] -= $this->discount_amount;
        if ($this->sort_order < $ot_subtotal->sort_order) {
          $order->info['subtotal'] -= $this->discount_amount;
        }
      }
    }
    
    function get_discount_amount($shipping_cost) {
      $discount = $shipping_cost * $this->discount_percentage;
      return $discount;
    }
    
    function get_discount_percentage($discount_table) {
      global $order;
      
      switch ($this->total_type) {
        case 'order-total':
          // remove the shipping cost from the calculation and return order total based on all other amounts
          $order_measure = $order->info['total'];
          break;
        case 'sub-total';
          $order_measure = $order->info['subtotal'];
          break;
      }
      $table_cost = $this->multi_explode(',', ':', $discount_table);
      for ($i=0; $i<count($table_cost); $i+=2) {
        if ($order_measure >= $table_cost[$i]) {
          $percentage = $table_cost[$i+1]; 
          $percentage = $percentage / 100; // 10 / 100 = 0.10
        }
      }
      return $percentage;
    }
    
    function build_shipping_modules_list() {
      $modules = explode(';', MODULE_SHIPPING_INSTALLED); // i.e. 0 => flat.php, 1 => zones.php
      $shipping_modules = array();
      foreach ($modules as $module) {
        $module_name = substr($module, 0, strrpos($module, '.'));
        $module = $module_name . '_' . $module_name; // flat_flat
        $shipping_modules[$module_name] = $module;
      }
      return $shipping_modules;
    }
    
	function multi_explode($delim1, $delim2, $string) {
  	  $new_data = array();
  	  $data = explode($delim1, $string);
  	  foreach ($data as $key => $value) {
  	    $new_data = array_merge($new_data, explode($delim2, $value));
	  }
	  return $new_data;
    }    
    
    function get_shipping_module($shipping_module, $module_id) {
      global $shipping_modules;
      if (!isset($GLOBALS["shipping_modules"])) {
        include_once(DIR_WS_CLASSES . 'shipping.php');
        $shipping_modules = new shipping($_SESSION['shipping']);
      }
      $selected_id = $this->get_shipping_module_id($module_id);
      $discount_table = false;
      $advshipper_methods = array();
      foreach ($shipping_module as $module_name => $id) {
        if ($this->advshipper($id)) {
          $quotes = $shipping_modules->quote();
          foreach ($quotes as $quote) {
            if ($quote['id'] == 'advshipper') {
              $advshipper_methods = $quote['methods'];
              break;
            }
          }
          foreach ($advshipper_methods as $advshipper_method) {
            $id = 'advshipper_' . $advshipper_method['id'];
            if ($id == $selected_id) {
              $discount_table = constant('MODULE_SHIPPING_DISCOUNTS_MODULE_' . strtoupper($module_name));
              return $discount_table;
            }
          }
        }
        //echo '<!-- ' . $id . ' == ' . $selected_id . ' -->' . "\n";
        if ($id == $selected_id) {
          $discount_table = constant('MODULE_SHIPPING_DISCOUNTS_MODULE_' . strtoupper($module_name));
          return $discount_table;
        }
      }
    }     
    
    function get_shipping_module_id($selected_id) {
      global $shipping_modules;
   
      if ($_GET['main_page'] == FILENAME_CHECKOUT_PROCESS) {
        include_once(DIR_WS_CLASSES . 'shipping.php');
        $shipping_modules = new shipping($_SESSION['shipping']);
      }
      
      $quotes = $shipping_modules->quote();

      $method_id = array();
      for ($i=0, $n=sizeof($quotes); $i<$n; $i++) {
        if ($quotes[$i]['module'] != '') {
          if (isset($quotes[$i]['error'])) {
            return 0;
          } else {
            for ($j=0, $n2=sizeof($quotes[$i]['methods']); $j<$n2; $j++) {
              // support for Advanced Shipper Module
              if ($this->advshipper($_SESSION['shipping']['id'])) {
                $method_id[] = "advshipper_" . $quotes[$i]['methods'][$j]['id'];
              } else {
                $method_id[] = $quotes[$i]['id'] . '_' . $quotes[$i]['id'];
                $method_id[] = $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'];
                $method_id[] = $quotes[$i]['methods'][$j]['id'] . '_' . $quotes[$i]['methods'][$j]['id']; 
              }
              //die();
              if (in_array($selected_id, $method_id)) {
                $this->method_id = $method_id[0];
                return $this->method_id; // example: flat_flat
              }
            }
          }
        }
      }
    }
    
    function advshipper($string) {
      $sub_string = substr($string, 0, 10);
      if ($sub_string == 'advshipper') {
        return true;
      } else {
        return false;
      }
    }
    
    function check() {
      global $db;
      if (!isset($this->check)) {
        $check_query = $db->Execute("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_SHIPPING_DISCOUNTS_STATUS'");
        $this->check = $check_query->RecordCount();
      }
      return $this->check;
    }
    
    function keys() {
      $keys = array('MODULE_SHIPPING_DISCOUNTS_STATUS', 'MODULE_SHIPPING_DISCOUNTS_TOTAL_TYPE', 'MODULE_SHIPPING_DISCOUNTS_SORT_ORDER');

      foreach ($this->shipping_mods as $module_name => $id) {
        $keys[] = 'MODULE_SHIPPING_DISCOUNTS_MODULE_' . strtoupper($module_name);
      } 
      return $keys;
    }
    
    function install() {
      global $db;
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Activate', 'MODULE_SHIPPING_DISCOUNTS_STATUS', 'true', 'Do you want to enable Shipping Discounts?', '6', '1','zen_cfg_select_option(array(\'true\', \'false\'), ', now())");     
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Total Type', 'MODULE_SHIPPING_DISCOUNTS_TOTAL_TYPE', 'sub-total', 'Should discounts be calculated based on sub-total (default) or order-total?', '6', '2','zen_cfg_select_option(array(\'sub-total\', \'order-total\'), ', now())");
      foreach ($this->shipping_mods as $module_name => $id) {
        $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Shipping Module " . ucwords($module_name) . "', 'MODULE_SHIPPING_DISCOUNTS_MODULE_" . strtoupper($module_name) . "', '', 'Create the discount table for the " . $module_name . " shipping module based on order sub-total.<br/>Example: 25:10,50:15,... With the first number being the order sub-total', '6', '20', now())");
      }
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_SHIPPING_DISCOUNTS_SORT_ORDER', '250', 'Sort order of display.', '6', '99', now())");
    }
    
    function remove() {
      global $db;
      $db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }
  }
?>
